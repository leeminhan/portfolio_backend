package sg.com.citi.porfoliobackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sg.com.citi.porfoliobackend.entities.Equity;
import sg.com.citi.porfoliobackend.entities.User;
import sg.com.citi.porfoliobackend.services.EquityService;
import sg.com.citi.porfoliobackend.services.UserService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping()
public class MainController {

    @Autowired
    private EquityService equityService;

    @Autowired
    private UserService userService;

    @GetMapping("/equity")
    @ResponseBody
    public ResponseEntity<List<Equity>> getAllEquities() {
        List<Equity> result = equityService.getAllEquities();
        if(result == null) {
            return ResponseEntity.notFound().build();
        }
        else {
            return ResponseEntity.ok().body(result);
        }
    }

    //get specific equity details
    @GetMapping("/equity/{ticker}")
    public ResponseEntity<Equity> getEquityByTicker(@PathVariable String ticker) {
        Equity result = equityService.getEquityByTicker(ticker);
        if(result != null) {
            return ResponseEntity.ok().body(result);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }


    //get all equity curr market price
    @GetMapping("/equity/currPrice")
    public ResponseEntity<Map> getAllCurrPrice() throws IOException {
        Map resultMap = equityService.getAllCurrPrice();
        if(resultMap != null) {
            return ResponseEntity.ok().body(resultMap);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    //get specific equity curr market price
    @GetMapping("/equity/currPrice/{ticker}")
    public ResponseEntity<Map> getCurrPrice(@PathVariable String ticker) throws IOException {
        Map resultMap = equityService.getCurrPrice(ticker);
        if(resultMap != null) {
            return ResponseEntity.ok().body(resultMap);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    //get specific user details
    @GetMapping("/user/{id}")
    public ResponseEntity<User> getUserById(@PathVariable int id) {
        User result = userService.getUserById(id);
        if(result != null) {
            return ResponseEntity.ok().body(result);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

}
