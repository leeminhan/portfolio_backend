package sg.com.citi.porfoliobackend.database;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import sg.com.citi.porfoliobackend.entities.Equity;
import sg.com.citi.porfoliobackend.entities.TradeOrder;
import sg.com.citi.porfoliobackend.repositories.EquityRepository;
import sg.com.citi.porfoliobackend.repositories.UserRepository;
import sg.com.citi.porfoliobackend.services.EquityService;

import java.util.List;

@Component
@EnableScheduling
public class ScheduledTask {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTask.class);

    @Autowired
    private EquityService equityService;
    private static int count = 0;

    @Value("${tradebackend.url}")
    private String tradeBackendUrl;

    @Scheduled(fixedDelay = 5000)
    public void getAllOrders() {
        RestTemplate rest = new RestTemplate();
        log.info("Getting trade orders from trade backend");

//        String url = "http://trade-backend-app-stock-trading-platform.singaporedevops7.conygre.com/";
        ResponseEntity<List<TradeOrder>> response = rest.exchange(tradeBackendUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<TradeOrder>>() {
        });

        List<TradeOrder> orders = response.getBody();

        calculatePortfolio(orders);


    }

    //this method parses the trade orders and updates the individual equities the user has
    public void calculatePortfolio(List<TradeOrder> orders) {
        System.out.println(orders);
        System.out.println("Beginning for loop, count = " + count);
        if(count >= orders.size()) {
            return;
        }
        else {
            for (int i = count; i < orders.size(); i++) {
                System.out.print("i in for loop: " + i);

                if (orders.get(i).getQuantity().equals(0.0) || orders.get(i).getPrice().equals(0.0) ||
                        orders.get(i).getPrice().equals(null) || orders.get(i).getQuantity().equals(null)) {
                    //skip out of this record, dont do anything to portfolio
                    break;
                } else {
                    if (orders.get(i).getTradeType().equals("BUY")) {
                        updateEquity(orders.get(i), 1);

                    }
                    if (orders.get(i).getTradeType().equals("SELL")) {
                        updateEquity(orders.get(i), -1);
                    }
                }
                //update count to last trade record
                count = i;
            }
            //make sure the next call starts from the newest added record
            count += 1;
            return;
        }
    }

    public void updateEquity(TradeOrder order, int direction) {
        //if user has no existing equity of this name, create new
        if (equityService.getEquityByTicker(order.getTicker()) == null) {
            if(direction == -1) {
                System.out.print("cannot sell equity you dont have");
                return;
            }
            if(direction == 1) {
                String ticker = order.getTicker();
                Double quantity = order.getQuantity();
                Double avgPrice = order.getPrice();
                Double costBasis = avgPrice * quantity;
                Equity newEquity = new Equity(ticker, quantity, avgPrice, costBasis);
                System.out.print("quantity from object: " + quantity);
                System.out.print("NEw quantity in equity" + newEquity.getQuantity());
                equityService.saveEquity(newEquity);
                log.info("new equity saved");
                return;
            }
        }

        //user has existing equity in portfolio
        if (equityService.getEquityByTicker(order.getTicker()) != null) {
            Equity thisEquity = equityService.getEquityByTicker(order.getTicker());
            System.out.print("this equity: " + thisEquity.getTicker());
            //add new order quantity to existing quantity; direction "BUY"
            Double oldQuantity = thisEquity.getQuantity();
            Double oldCostBasis = thisEquity.getCostBasis();
            Double oldAvgPrice = thisEquity.getAvgPrice();
            Double newCostBasis = oldCostBasis + (order.getPrice() * order.getQuantity() * direction);
            //will minus if sell, plus if buy
            Double newQuantity = oldQuantity + (order.getQuantity() * direction);
            //delete the equity in portfolio if all sold
            if(newQuantity <= 0 ) {
                equityService.deleteEquity(thisEquity);
                return;
            }
            else {
                Double newAvgPrice = newCostBasis / (newQuantity);
                System.out.println("newAvgPrice in: " + newAvgPrice);
                thisEquity.setQuantity(newQuantity);
                thisEquity.setAvgPrice(newAvgPrice);
                thisEquity.setCostBasis(newCostBasis);
                equityService.saveEquity(thisEquity);
                System.out.print("existing equity updated" + thisEquity);
                return;
            }
        }
    }


}
