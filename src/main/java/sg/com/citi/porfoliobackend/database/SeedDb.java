package sg.com.citi.porfoliobackend.database;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class SeedDb {

    @Autowired
    JdbcTemplate template;

    @Autowired
    public void SeedDb(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("insert into user (first_name, last_name, cash_amount) values (?, ?, ?)",new Object[]{"Andy", "Olsen", 500});
        jdbcTemplate.update("insert into equity (ticker, quantity, avg_price, cost_basis) values (?, ?, ?, ?)",new Object[]{"AAPL", 10.0, 100.0, 1000.0});
        jdbcTemplate.update("insert into equity (ticker, quantity, avg_price, cost_basis) values (?, ?, ?, ?)",new Object[]{"GOOG", 10.0, 2000.0, 20000.0});
    }

}


