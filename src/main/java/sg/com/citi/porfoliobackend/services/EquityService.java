package sg.com.citi.porfoliobackend.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import sg.com.citi.porfoliobackend.entities.Equity;
import sg.com.citi.porfoliobackend.repositories.EquityRepository;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class EquityService {

    @Autowired
    private EquityRepository equityRepo;

    @Value("${token}")
    private String api_key;
    @Value("${api_key}")
    private String crypto_key;

    @Value("${quote_url}")
    private String quote_url;

    @Value("${crypto_url}")
    private String crypto_url;

    public List<Equity> getAllEquities() {
        try {
            return equityRepo.getDistinctByQuantityNotNull();
        }
        catch (Exception e) {
            System.out.println("Unable to get Equities" + e.getMessage());
            return null;
        }

    }

    public Equity getEquityByTicker(String ticker) {
        try {
            return equityRepo.findByTicker(ticker);
        }
        catch(Exception e) {
            System.out.println("Unable to find equity with: " + ticker + e.getMessage() );
            return null;
        }
    }

    public void saveEquity(Equity equity) {
        equityRepo.save(equity);
    }

    public void deleteEquity(Equity deletedEquity) {
        try {
            equityRepo.delete(deletedEquity);
        }
        catch(Exception e) {
            System.out.println("Error deleting" + e.getMessage());
        }
    }


    public Map<String,Double> getAllCurrPrice () throws IOException {
        List<Equity> allEquities = equityRepo.getDistinctByQuantityNotNull();
        Map<String,Double> map = new HashMap<>();
        for (int i = 0; i < allEquities.size(); i++) {
            String ticker = allEquities.get(i).getTicker();
            Map<String,Double> newMap = getCurrPrice(ticker);
            map.put(ticker,newMap.get(ticker));
        }
        return map;
    }

    public Map<String,Double> getCurrPrice (String ticker) throws IOException {
        Map<String,Double> map = new HashMap<>();
        RestTemplate rest = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        String thisUrl = quote_url + ticker + "&token=" + api_key;
        ResponseEntity<String> response = rest.getForEntity(thisUrl,String.class);
        JsonNode node = mapper.readValue(response.getBody(),JsonNode.class);
        JsonNode priceNode = node.get("c");
        Double price = priceNode.asDouble();
        //workaround for crypto ticker
        if(price == 0.0) {
            String crypto = crypto_url + ticker + "&apikey=" + crypto_key;
            ResponseEntity<String> cryptoResponse = rest.getForEntity(crypto,String.class);
            JsonNode cryptoNode = mapper.readValue(cryptoResponse.getBody(),JsonNode.class);
            JsonNode cryptoPriceNode = cryptoNode.at("/Global Quote/05. price");
            Double cryptoPrice = cryptoPriceNode.asDouble();
            price = cryptoPrice;
        }

        System.out.println("price of ticker:" + price);
        map.put(ticker,price);
        return map;
    }






}
