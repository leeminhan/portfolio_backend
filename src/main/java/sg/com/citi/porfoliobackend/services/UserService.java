package sg.com.citi.porfoliobackend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sg.com.citi.porfoliobackend.entities.User;
import sg.com.citi.porfoliobackend.repositories.UserRepository;

import java.util.Collection;


@Component
public class UserService {

    @Autowired
    private UserRepository userRepo;

    public User getUserById(int id) {
        try {
            return userRepo.getById(id);
        }
        catch (Exception e) {
            System.out.println("Error retrieving user by Id" + e.getMessage());
            return null;
        }
    }


}
