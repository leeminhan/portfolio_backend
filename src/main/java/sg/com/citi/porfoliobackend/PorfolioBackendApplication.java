package sg.com.citi.porfoliobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class PorfolioBackendApplication {

	public static void main(String[] args) {

		SpringApplication.run(PorfolioBackendApplication.class, args);
	}

}
