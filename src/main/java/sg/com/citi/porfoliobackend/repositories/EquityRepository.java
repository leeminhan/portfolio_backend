package sg.com.citi.porfoliobackend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sg.com.citi.porfoliobackend.entities.Equity;

import java.util.List;

@Repository
public interface EquityRepository extends JpaRepository<Equity,Integer> {

    Equity findByTicker(String ticker);
    Equity findById(int id);
    List<Equity> getDistinctByQuantityNotNull();


}
