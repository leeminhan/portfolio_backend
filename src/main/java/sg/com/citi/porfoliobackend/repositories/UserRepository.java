package sg.com.citi.porfoliobackend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sg.com.citi.porfoliobackend.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    User getById(int id);
}
