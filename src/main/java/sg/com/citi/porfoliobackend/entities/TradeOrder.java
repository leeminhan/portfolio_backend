package sg.com.citi.porfoliobackend.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter @NoArgsConstructor @ToString
public class TradeOrder implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String tradeType;
    private String ticker;
    private Double price;
    private Double quantity;
    @Column(name = "created_at")
    private String created;
    private String tradeState;
    private String tradeExchange;

    public TradeOrder(int id, String tradeType, String ticker, Double price, Double quantity, String created_at, String tradeState, String tradeExchange) {
        this.id = id;
        this.tradeType = tradeType;
        this.ticker = ticker;
        this.price = price;
        this.quantity = quantity;
        this.created = created_at;
        this.tradeState = tradeState;
        this.tradeExchange = tradeExchange;
    }

    @Override
    public String toString() {
        return "TradeOrder{" +
                "id=" + id +
                ", tradeType='" + tradeType + '\'' +
                ", ticker='" + ticker + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", created='" + created + '\'' +
                ", tradeState='" + tradeState + '\'' +
                ", tradeExchange='" + tradeExchange + '\'' +
                '}';
    }


}
