package sg.com.citi.porfoliobackend.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity @Getter @Setter @NoArgsConstructor
public class Equity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String ticker;
    private Double quantity;
    private Double avgPrice;
    private Double costBasis;

    public Equity(String ticker, Double quantity, Double avgPrice, Double costBasis) {
        this.ticker = ticker;
        this.quantity = quantity;
        this.avgPrice = avgPrice;
        this.costBasis = costBasis;

    }






}
